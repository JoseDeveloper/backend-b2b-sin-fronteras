<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Question;
use App\Models\UserAnswer;


class EvaluationUser extends Model
{
    protected $table = 'evaluation_user';
    protected $fillable = [
        'id',
        'evaluation_id',
        'evaluator_user_id',
        'evaluated_user_id',
        'presented',
        'user_score'
    ];


    public function assign_evaluation_to_user($user_id, $user_id_assign, $evaluation_id, $mode, $id)
    {
        $objectSave = [
            'evaluation_id' => $evaluation_id,
            'evaluator_user_id' => $user_id,
            'evaluated_user_id' => $user_id_assign
        ];

        if ($mode == 1) {
            $update = EvaluationUser::find($id)->update($objectSave);
            $response = EvaluationUser::where('id', $id)->first();

        } else {
            $response = EvaluationUser::create($objectSave);
        }

        return $response;
    }

    public function send_qualification_to_user($id, $qualification)
    {
        $update = EvaluationUser::find($id)->update(['user_score' => $qualification]);
        $response = EvaluationUser::where('id', $id)->first();
        return $response;
    }

    public function get_evaluations_by_evaluator_user($user_id)
    {
        $result = EvaluationUser::where('evaluator_user_id', $user_id)
            ->leftJoin('evaluations', 'evaluations.id', '=', 'evaluation_user.evaluation_id')
            ->select(
                'evaluation_user.*',
                'evaluation_user.user_score as userScore',
                'evaluations.name as evaluationName',
                'evaluations.score as evaluationScore',
                'evaluations.created_at as evaluationDate'
            )
            ->get();

        foreach ($result as $ev) {
            $questions = Question::where('evaluation_id', $ev->evaluation_id)->get();

            foreach ($questions as $q) {
                $q->answers_decode = json_decode($q->answers);
                $user_answers = UserAnswer::where('question_id', $q->id)->first();
                $user_answers['user_answers'] = json_decode($user_answers['user_answers']);
                $q->user_answers = $user_answers;
            }

            $ev->questions = $questions;
        }

        return $result;
    }

    public function get_evaluations_by_evaluated_user($user_id)
    {
        $result = EvaluationUser::where('evaluated_user_id', $user_id)
            ->leftJoin('evaluations', 'evaluations.id', '=', 'evaluation_user.evaluation_id')
            ->select(
                'evaluation_user.*',
                'evaluation_user.user_score as userScore',
                'evaluations.name as evaluationName',
                'evaluations.score as evaluationScore',
                'evaluations.created_at as evaluationDate'
            )
            ->get();

        foreach ($result as $ev) {
            $questions = Question::where('evaluation_id', $ev->evaluation_id)->get();

            foreach ($questions as $q) {
                $q->answers_decode = json_decode($q->answers);
                $user_answers = UserAnswer::where('question_id', $q->id)->first();
                $user_answers['user_answers'] = json_decode($user_answers['user_answers']);
                $q->user_answers = $user_answers;
            }

            $ev->questions = $questions;
        }

        return $result;
    }

    public function check_evaluation_user($id)
    {
        $update = EvaluationUser::find($id)->update(['presented' => '1']);
        $response = EvaluationUser::where('id', $id)->first();
        return $response;
    }
}
