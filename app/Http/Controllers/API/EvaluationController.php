<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Evaluation;
use App\Models\EvaluationUser;
use App\Models\Question;
use App\Models\UserAnswer;
use App\User;

class EvaluationController extends Controller
{
    public static function get_evaluations(Request $request)
    {
        $evaluation = new Evaluation;
        $evaluations = $evaluation->get_evaluations();

        return response()->json([
            'data' => $evaluations,
        ]);
    }
    
    public function get_all_evaluations(Request $request)
    {
        $evaluation = new Evaluation;
        $evaluations = $evaluation->get_all_evluations();

        return response()->json([
            'data' => $evaluations,
        ]);
    }


    public function get_evaluations_by_evaluator_user(Request $request)
    {
        $user_id = $request->input('user_id');
        $evaluation_user = new EvaluationUser;
        $evaluation = new Evaluation;
        $user = new User;
        $evaluations_user = $evaluation_user->get_evaluations_by_evaluator_user($user_id);
        $users = $user->get_users();
        $evaluations = $evaluation->get_evaluations();

        return response()->json([
            'data' => $evaluations_user,
            'users' => $users,
            'evaluations' => $evaluations
        ]);
    }

    public function get_evaluations_by_evaluated_user(Request $request)
    {
        $user_id = $request->input('user_id');
        $evaluation_user = new EvaluationUser;
        $evaluations_user = $evaluation_user->get_evaluations_by_evaluated_user($user_id);

        return response()->json([
            'data' => $evaluations_user,
        ]);
    }

    public function assign_evaluation_to_user(Request $request)
    {
        $user_id = $request->input('user_id');
        $user_id_assign = $request->input('user_id_assign');
        $evaluation_id = $request->input('evaluation_id');
        $mode = $request->input('mode');
        $id = $request->input('id');
        $evaluation_user = new EvaluationUser;
        $evaluations_user = $evaluation_user->assign_evaluation_to_user($user_id, $user_id_assign, $evaluation_id, $mode, $id);
        $evaluator_data_user = $evaluation_user->get_evaluations_by_evaluator_user($user_id);

        return response()->json([
            'data' => $evaluator_data_user,
        ]);
    }

    public function send_qualification_to_user(Request $request)
    {
        $id = $request->input('evaluation_user_id');
        $qualification = $request->input('qualification');
        $user_id = $request->input('user_id');
        $evaluation_user = new EvaluationUser;
        $evaluations_user = $evaluation_user->send_qualification_to_user($id, $qualification);

        $evaluator_data_user = $evaluation_user->get_evaluations_by_evaluator_user($user_id);

        return response()->json([
            'data' => $evaluator_data_user,
        ]);
    }



    public function save_evaluation(Request $request)
    {
        $evaluation = new Evaluation;
        $question = new Question;
        $eval_id = $request->input('evaluation_id');
        $evaluation_name = $request->input('evaluation_name');
        $questions_and_answers = $request->input('questions_and_answers');

        if($eval_id == '') { 
            $id_inserted_evaluation = $evaluation->insert_evaluation($evaluation_name);
        }

        $evaluation_id = ($eval_id) ? $eval_id : $id_inserted_evaluation;

        foreach ($questions_and_answers as $qa) {
            $qa['evaluation_id'] = $evaluation_id;
            $question->insert_question($qa);
        }


        $evaluations = $evaluation->get_evaluations();

        return response()->json([
            'actualizedData' => $evaluations,
        ]);
    }

    public function save_user_response_evaluation(Request $request)
    {
        $user_id = $request->input('user_id');
        $questions = $request->input('questions');
        $id = $request->input('id');

        $user_answers = new UserAnswer;
        $evaluation_user = new EvaluationUser;

        foreach ($questions as $q) {
            if (isset($q['answers_decode'][0]['userResponse'])) {
                $objectSave = [
                    'user_id' => $user_id,
                    'question_id' => $q['id'],
                    'user_answers' => json_encode($q['answers_decode']),
                ];

                $user_answers->insert_user_answer($objectSave);
            }

            if (isset($q['answers_decode'])) {
                $objectSave = [
                    'user_id' => $user_id,
                    'question_id' => $q['id'],
                    'user_answers' => json_encode($q['answers_decode']),
                ];

                $user_answers->insert_user_answer($objectSave);
            }
        }

        $evaluation_user->check_evaluation_user($id);
        $evaluations_user = $evaluation_user->get_evaluations_by_evaluated_user($user_id);

        return response()->json([
            'data' => $evaluations_user,
        ]);
    }
}
