<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\CategoryProduct;

class CategorieProductController extends Controller
{
    public function get_categories()
    {
        $not_found = [
            'message'=>'Not Found'
        ];

        $category = new CategoryProduct();
        $category = CategoryProduct::all();
        if(is_null($category)){
            return response()->json($not_found,404);
        }
        return response()->json($category,200);
    }

    public function get_categories_active()
    {
        $not_found = [
            'message'=>'Not Found'
        ];

        $category = new CategoryProduct();
        $category = CategoryProduct::where('is_active', 1)->get();
        if(is_null($category)){
            return response()->json($not_found,404);
        }
        return response()->json($category,200);
    }

    public function save_category(Request $request)
    {
        $id = $request->input('id');
        $description = $request->input('description');
        $url_image = $request->input('url_image');
        $is_active = $request->input('is_active');


        $objectSave = [
            'description' => $description,
            'url_image' => $url_image,
            'is_active' => $is_active
        ];
        

        if($id != 'null'){
            $category = CategoryProduct::findOrFail($id);
            $category->update($objectSave);
        }else{
            $category = CategoryProduct::create($objectSave);
        }
        $data = CategoryProduct::all();
        return response()->json($data,201);
    }

    public function delete_category($id)
    {
        $category = CategoryProduct::find($id);
        $not_found = [
            'message'=>'Not Found'
        ];

        if(is_null($category)){
            return response()->json($not_found,404);
        }

        $category->delete();
        $data = CategoryProduct::all();

        return response()->json([
            "error" => "",
            "response" => $category,
            "data" => $data
        ]);
        
    }

    public function active_category($id, $action)
    {
        $vacant = CategoryProduct::find($id)->update(['is_active' => $action]);

        return response()->json($vacant, 200);
    }
}
