<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAnswer extends Model
{
    protected $table = 'user_answers';
    protected $fillable = [
        'id',
        'user_id',
        'question_id',
        'user_answers',
        'created_at',
        'updated_at'
    ];

    public function insert_user_answer($objectSave)
    {
       
        $rowCreated = UserAnswer::create($objectSave);
        return $rowCreated->id;
    }
}
