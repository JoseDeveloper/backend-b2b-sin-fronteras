<?php

namespace App\Http\Controllers\API;

use App\Models\Category;
use App\Models\City;
use App\Models\PasseUser;
use App\Models\Provinces;
use http\Message;
use Illuminate\Foundation\Console\PackageDiscoverCommand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Socialite;

class UserController extends Controller
{

    // use AuthenticatesUsers;
    public $successStatus = 200;

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password

     */
    public function signup(Request $request)
    {
        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role_id' => '5'
        ]);

        $user->save();
        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }

    public function signupClient(Request $request)
    {
        $user = new User([
            'name' => $request->name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role_id' => '5'
        ]);

        $user->save();
        $user->id;
        return $this->login($request);
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        try {
            $request->validate([
                'email' => 'required|string|email',
                'password' => 'required|string'
                // 'remember_me' => 'boolean'
            ]);

            $credentials = request(['email', 'password']);

            if (!Auth::attempt($credentials))
                return response()->json([
                    'message' => 'Unauthorized',
                ], 401);

            $user = $request->user();
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;

            if ($request->remember_me)
                $token->expires_at = Carbon::now()->addWeeks(1);

            $token->save();

            $user = $request->user();
            $user_data = $user->data(Auth::id());


            return response()->json([
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString(),
                'userData' => $user_data
            ]);
        } catch (\Throwable $e) {
            return "ERROR" . " " . $e;
        }
    }

    public function loginGoogle(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            // 'password' => 'required|string'
            // 'remember_me' => 'boolean'
        ]);


        $user_found = User::where('email', $request->input('email'))->first();
        if ($user_found) {
            Auth::login($user_found);
            $token = Auth::user()->createToken('Personal Access Token')->accessToken;
            return response()->json([
                'access_token' => $token,
                'token_type' => 'Bearer',
                'userData' => $user_found,
            ]);
        } else {
            $user = new User;
            $user->name = $request->input('name');
            $user->role_id = '5';
            $user->email = $request->input('email');
            $user->password = bcrypt($user->name);
            $user->image_profile = $user->image_profile;
            $user->save();
            $user_found = User::where('email', $request->input('email'))->first();
            Auth::login($user_found);
            $token = Auth::user()->createToken('Personal Access Token')->accessToken;

            return response()->json([
                'access_token' => $token,
                'token_type' => 'Bearer',
                'userData' => $user_found,
            ]);
        }
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {

        $user = $request->user();
        $roles_user = $user->roles();

        return response()->json([
            "userRoles" => $roles_user
        ]);
    }

    public function getUser(Request $request)
    {

        $user = $request->user();

        return response()->json([
            "user" => $user,
        ]);
    }

    public function get_users()
    {
        $user = new User;
        $response = $user->get_users();
        return response()->json([
            "data" => $response,
        ]);
    }

    

    
}
