<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';
    protected $fillable = [
        'id',
        'type',
        'evaluation_id',
        'question',
        'answers',
        'status'
    ];

    public function insert_question($data)
    {
       
        $objectSave = [
            'type' => $data['type'],
            'evaluation_id' => $data['evaluation_id'],
            'question' => $data['question'],
            'answers' => json_encode($data['answers'])
        ];

        $rowCreated = Question::create($objectSave);
        return $rowCreated->id;
    }

    
}
