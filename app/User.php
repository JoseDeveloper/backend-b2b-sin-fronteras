<?php

namespace App;

use App\Models\Passe;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;
use App\Role;

class User extends Authenticatable
{

    use HasApiTokens,
        Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'email',
        'resume', 'province', 'secondary_street',
        'main_street', 'neighborhood', 'mobile',
        'image_profile', 'city', 'password',
        'current_level_id', 'gym_id', 'branch_id',
        'role_id', 'lead','image_before','image_after','description','activities'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function data($id)
    {
        $user_roles = User::where('users.id', $id)
            ->join('roles', 'users.role_id', '=', 'roles.id')
            ->select('users.*', 'roles.name as roleName', 'users.name as userName')->first();
        return $user_roles;
    }

    public function get_users()
    {
        $result = User::where('role_id','!=','1')->get();
        return $result;
    }


}
